const { I } = inject();

module.exports = 
{
        tripPlanTab: {css: '#tab-pane-tp'},
        fromStop: {css: '#tniFromTripLocation'},
        toStop: {css: '#tniToTripLocation'},
        goButton: {css: 'div tni-trip-search-form .trip-planner-buttons-panel button.go-btn'},
        lblPublicTransport: {css: '#trip-transport-filter-tab__public-transport > label:nth-child(1)'},
        resultList: {css: "div [card-header=''] .summary-footer"},
        siteURL: 'https://transportnsw.info/',
                
};