const { I, pageObj, testData } = inject();

Feature('TripPlanning');
Data(testData.trainStopData).Scenario('Test trip search between stations', (I,current) => {
I.amOnPage(pageObj.siteURL)
I.say('I am on  "Trip Planner" tab');
I.click(pageObj.tripPlanTab);
I.see("Trip Planner");
I.say('Entering "From station" data');
I.fillField(pageObj.fromStop, current.FromStop);
I.say('Entering "To  station" data');
I.fillField(pageObj.toStop, current.ToStop);
I.say('Click on "Go" to search');
I.click(pageObj.goButton);
I.say('Check for results if it contains "Public transport"');
I.see("Public transport",pageObj.lblPublicTransport);
I.say('Check for results if it contains 4 records');
I.waitNumberOfVisibleElements(pageObj.resultList,4,6)

});
