const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// HEADLESS=true npx codecept run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './test/*_test.js',
  output: './output',
  helpers: {
    WebDriver: {
      url: 'localhost',
      browser: 'firefox',
      desiredCapabilities: {
        "moz:firefoxOptions": {
          "args": [
            "--headless"        ]
        }
      }

    }
  },
  include: {
    I: './steps_file.js',
    pageObj: './pageObject/pageElement.js',
    testData: './testData/testDataSearch.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'GUI-CodeceptJS-SeleniumServer',
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    desiredCapabilities: {
      // close all unexpected popups (rase added)
      unexpectedAlertBehaviour: 'dismiss',
    },
    stepByStepReport: {  //(rase added)
      "enabled": false,
      "deleteSuccessful": false
    }
  }
}