
let trainStopData = new DataTable(['FromStop', 'ToStop']); 
trainStopData.add(['Rockdale Station, Rockdale', 'Hurstville Station, Hurstville']); 
trainStopData.add(['Town Hall Station, Sydney', 'Rockdale Station, Rockdale']);

module.exports={trainStopData}

// module.exports = 
// {
//         fromStop: 'Rockdale Station, Rockdale',
//         toStop: 'Hurstville Station, Hurstville'
// };